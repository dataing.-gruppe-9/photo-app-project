# Photo App Project

Photo App Project i Systemutvikling.

Photo App Project is an app focused on making it easier to handle your photos locally.
It is written completely in Java for easy use across different operating systems. The app is
capable of handeling different types of picture files in an easy way with the ability to 
export them into a pdf file.


## Installation

https://gitlab.stud.idi.ntnu.no/dataing.-gruppe-9/photo-app-project/-/wikis/uploads/Installation_manual.pdf


## Built with

IntelliJ IDEA - Used for coding