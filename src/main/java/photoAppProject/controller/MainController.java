package photoAppProject.controller;

import javafx.scene.control.TextInputDialog;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import photoAppProject.model.ImageArchive;
import photoAppProject.model.Picture;
import photoAppProject.model.MetaData;
import photoAppProject.view.ImageArchiveTableView;
import photoAppProject.view.ImageDialogue;
import photoAppProject.view.PDFDialogue;
import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * Main photoAppProject.controller to control most of the logics
 */
public class MainController {

  private final Logger logger;

  /**
   * constructor for an instance of a MainController
   */
  public MainController(){
    this.logger = Logger.getLogger(this.getClass().getName());
  }

  /**
   * Adds picture to database
   * @param imageArchive imagearchive that stores the pictures
   * @param stage main stage
   * @param tableView tableview to update
   */
  public void addPicture(ImageArchive imageArchive, Stage stage, ImageArchiveTableView tableView) {
    FileChooser fileChooser = new FileChooser();
    File selectedFile = fileChooser.showOpenDialog(stage);
    String picturePath = selectedFile.getAbsolutePath();
    imageArchive.addPicture(picturePath);
    tableView.updateObservableList();

  }

  /**
   * Creates a pdf from a ArrayList of pictures.
   *
   * @param archive Archive of which you want to print pictures from
   */
  public void createPdf(ImageArchive archive) {
    PDFDialogue pdfDialogue = new PDFDialogue(archive);
    pdfDialogue.showAndWait();

  }

  /**
   * Collects information on which search method to use.
   *
   * @param parent       MainWindow, does not have a use yet.
   * @param imageArchive ArrayList containing all Picture elements stored in the database.
   */
//  public void search(MainWindow parent, ImageArchive imageArchive) {
//    ArrayList<Picture> pictures = imageArchive.getPictures();
//    Alert searchType = new Alert(Alert.AlertType.CONFIRMATION);
//    searchType.setTitle("Search type dialogue");
//    searchType.setHeaderText("Please choose a search method");
//
//    ButtonType tags = new ButtonType("Tags");
//    ButtonType metaData = new ButtonType("Metadata");
//
//    searchType.getButtonTypes().setAll(tags, metaData);
//
//    ArrayList<Picture> filteredList;
//
//    Optional<ButtonType> result = searchType.showAndWait();
//    if (result.get() == tags) {
//      filteredList = searchWithTags(pictures);
//    } else if (result.get() == metaData) {
//      filteredList = searchWithMetadata(pictures);
//    }
//
//
//  }

  /**
   * Searches the full list of pictures for the entered tag.
   *
   * @param pictures Arraylist containing all Picture objects contained in the database.
   * @return returns Arraylist with all pictures containing the tag that
   *        has been entered into textinputdialog.
   */
//  public ArrayList<Picture> searchWithTags(ArrayList<Picture> pictures) {
//    TextInputDialog getTag = new TextInputDialog();
//    getTag.setTitle("Dialog to get search tag");
//    getTag.setContentText("Please enter the tag you want to search with");
//
//    final String tag;
//
//    Optional<String> result = getTag.showAndWait();
//    if (result.isPresent()) {
//      tag = result.get().toUpperCase();
//    } else {
//      throw (new NullPointerException("Input dialogue did not return a String"));
//    }
//
//    ArrayList<Picture> filteredList = new ArrayList<>();
//    pictures.forEach(picture -> {
//      if (picture.containsTag(tag)) {
//        filteredList.add(picture);
//      }
//    });
//
//    filteredList.forEach(picture -> System.out.println("Sss"));
//    return filteredList;
//  }

  /**
   * Opens dialog window dispalying images.
   *
   * @param tableView The tableView to get the selected item.
   */
  public void showPicture(ImageArchiveTableView tableView) {
    Picture image = tableView.getTableView().getSelectionModel().getSelectedItem();

    ImageDialogue.showImageDialogue(image);
  }

  /**
   * Searches the archive for metadata, tags and title.
   *
   * @param searchWord the string to use in the search.
   * @param archive    the archive to search in.
   * @param tableView  the tableview to update when search is done.
   */
  public void doSearchAll(String searchWord,
                          ImageArchive archive,
                          ImageArchiveTableView tableView) {
    if (searchWord == null || searchWord.trim().length() == 0) {
      throw new IllegalArgumentException("There is no search word");
    }
    List foundPictures = archive.anySearch(searchWord);
    tableView.setObservableList(foundPictures);

  }

  /**
   * Opens add tag dialogue and adds tag to selected image
   *
   * @param tableView tableview to get picture object from and to update
   * @param imageArchive The archive that holds the picture you want to add tags to
   */
  public void doAddTag(ImageArchiveTableView tableView, ImageArchive imageArchive) {
    TextInputDialog addTag = new TextInputDialog();
    addTag.setTitle("Add a tag");
    addTag.setContentText("Write in a tag to add to the selected picture.");

    Optional<String> result = addTag.showAndWait();
    if (result.isPresent() && result.get().trim().length() != 0) {
      String tag = result.get();
      Picture picture = tableView.getTableView().getSelectionModel().getSelectedItem();
      imageArchive.addTag(picture,tag);
    } else {
      throw new NullPointerException("No tag was added");
    }
  }


  /**
   * Removes a picture from the database and updates the observable list
   *
   * @param tableView tableview to get picture object from and to update
   * @param imageArchive archive storing the pictures
   */
  public void removePicture(ImageArchiveTableView tableView, ImageArchive imageArchive) {
    Picture picture = tableView.getTableView().getSelectionModel().getSelectedItem();
    imageArchive.remove(picture);
    tableView.updateObservableList();

    }
}
