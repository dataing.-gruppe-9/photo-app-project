package photoAppProject.view;

import java.util.Iterator;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import photoAppProject.model.Picture;

/**
 * Class for displaying images and their data
 */
public class ImageDialogue {

  /**
   * Opens a new window to display image and image metadata.
   *
   * @param image - Image to display
   */
  public static void showImageDialogue(Picture image) {
    Stage imageDialogue = new Stage();

    Button close = new Button("Close");
    close.setOnAction(event -> imageDialogue.close());

    Image image1 = new Image("file:"+image.getPath());

    ImageView imageView = new ImageView();
    imageView.setImage(image1);

    if(image1.getHeight() >= 1080 || image1.getWidth() >= 1920){
      imageView.setFitHeight(720);
      imageView.setFitWidth(((image1.getWidth()/image1.getHeight())*720));

    }

    GridPane gridPane = new GridPane();
    if (image.getMetaData() != null) {
      Iterator<String> metadataIt = image.getMetaData().stringMetadata().iterator();
      gridPane.add(new Label("Metadata: "), 0, 0);
      int i = 1;
      while (metadataIt.hasNext()) {

        String metadata = metadataIt.next();
        gridPane.add(new Label(metadata), 0, i);
        i++;
      }
    }

    if (!image.getTags().isEmpty()) {
      gridPane.add(new Label("Tags: "), 1, 0);
      Iterator<String> tagsIt = image.getTags().iterator();
      int i;
      i = 1;
      while (tagsIt.hasNext()) {
        String tag = tagsIt.next();
        gridPane.add(new Label(tag), 1, i);
        i++;
      }
    }

    VBox vbox = new VBox();
    vbox.getChildren().addAll(gridPane,close);
    BorderPane pane = new BorderPane();
    pane.setCenter(imageView);
    pane.setBottom(vbox);
    pane.setPadding(new Insets(50,100,100,100));

    Scene scene = new Scene(pane);
    imageDialogue.setScene(scene);
    imageDialogue.showAndWait();
  }

    /**
     * Opens a new window to display image and image metadata.
     *
     * @param image - Image to display
     */
    public static void showImageDialogue1(Picture image) {
        Stage imageDialogue = new Stage();

        Button close = new Button("Close");
        close.setOnAction(event -> imageDialogue.close());

        Image image1 = new Image("file:"+image.getPath());

        ImageView imageView = new ImageView();
        imageView.setImage(image1);

        if(image1.getHeight() >= 1080 || image1.getWidth() >= 1920){
            imageView.setFitHeight(720);
            imageView.setFitWidth(((image1.getWidth()/image1.getHeight())*720));

        }

        GridPane gridPane = new GridPane();
        if (image.getMetaData() != null) {
            Iterator<String> metadataIt = image.getMetaData().stringMetadata().iterator();
            gridPane.add(new Label("Metadata: "), 0, 0);
            int i = 1;
            while (metadataIt.hasNext()) {

                String metadata = metadataIt.next();
                gridPane.add(new Label(metadata), 0, i);
                i++;
            }
        }

        if (!image.getTags().isEmpty()) {
            gridPane.add(new Label("Tags: "), 1, 0);
            Iterator<String> tagsIt = image.getTags().iterator();
            int i;
            i = 1;
            while (tagsIt.hasNext()) {
                String tag = tagsIt.next();
                gridPane.add(new Label(tag), 1, i);
                i++;
            }
        }

        VBox vbox = new VBox();
        vbox.getChildren().addAll(gridPane,close);
        BorderPane pane = new BorderPane();
        pane.setCenter(imageView);
        pane.setBottom(vbox);
        pane.setPadding(new Insets(50,100,100,100));

        Scene scene = new Scene(pane);
        imageDialogue.setScene(scene);
        imageDialogue.showAndWait();
    }
}
