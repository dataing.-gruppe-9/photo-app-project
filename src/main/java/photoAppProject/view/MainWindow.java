package photoAppProject.view;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Modality;
import javafx.util.Pair;
import photoAppProject.controller.MainController;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import photoAppProject.model.ImageArchiveAL;
import photoAppProject.model.ImageArchiveDAO;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.stage.Stage;
import photoAppProject.model.ImageArchive;
import photoAppProject.model.Picture;

/**
 * Main class of PhotoApp
 * Most of the UI
 */
public class MainWindow extends Application {

  MainController mainController;
  Logger logger;
  ImageArchive pictures;

  public static void main(String[] args) {
    launch(args);
  }

  /**
   * Opens the login dialog which takes in the login credentials to NTNU
   * MYSQL database
   *
   * @return Pair of strings with password as key and username as value
   **/
  public Pair<String, String> getLogin() {
    Dialog<Pair<String,String>> loginDialog = new Dialog<>();
    loginDialog.setTitle("Login");
    loginDialog.setHeaderText("Please enter your NTNU MYSQL password and username");

    ButtonType loginButtonType = new ButtonType("login", ButtonBar.ButtonData.OK_DONE);
    loginDialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

    GridPane grid = new GridPane();

    TextField username = new TextField();
    username.setPromptText("Username");
    PasswordField password = new PasswordField();
    password.setPromptText("Password");

    grid.add(new Label("Username"),0,0);
    grid.add(username, 1, 0);
    grid.add(new Label("Password"),0,1);
    grid.add(password,1,1);
    grid.add(new Label("If you want to use the default login press cancel"),0,2);
    loginDialog.getDialogPane().setContent(grid);

    Node loginButton = loginDialog.getDialogPane().lookupButton(loginButtonType);
    loginButton.setDisable(true);

    password.textProperty().addListener((observable, oldValue, newValue) -> {
      if(!newValue.trim().isEmpty()) {
        loginButton.setDisable(false);
      } else {
        loginButton.setDisable(true);
      }
    });

    loginDialog.setResultConverter(dialogButton -> {
      if (dialogButton == loginButtonType) {
        return new Pair<>(password.getText(), username.getText());
      }
      return null;
    });

    Optional<Pair<String,String>> result = loginDialog.showAndWait();

    if(result.isPresent()) {
      return result.get();
    } else {
      return null;
    }
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    Alert defaultLogin = new Alert(Alert.AlertType.INFORMATION);
    boolean showDefaultLogin = false;

    //Displays the getlogin dialog, and makes the default login alert
    //if the logincredentials from the getLogin dialogs are invalid
    try {
      Pair<String,String> login = getLogin();
      if(!login.getKey().trim().isEmpty() && !login.getValue().trim().isEmpty()) {
        this.pictures = new ImageArchiveDAO(login.getValue(), login.getKey());
      } else {
        throw new Exception();
      }
    } catch (Exception e) {
        this.pictures = new ImageArchiveDAO();
        defaultLogin.setTitle("Default login");
        defaultLogin.setHeaderText("You have been logged into the default database");
        defaultLogin.setContentText("You have put in an invalid password and username combination");
        defaultLogin.initModality(Modality.APPLICATION_MODAL);
        showDefaultLogin = true;
    }

    primaryStage.setTitle("Photo App");

    AnchorPane anchorPane = new AnchorPane();
    BorderPane rootPane = new BorderPane();

    //Gets the tableView
    ImageArchiveTableView tableView = new ImageArchiveTableView(pictures);
    VBox tableViewVbox = tableView.centerContent();
    tableViewVbox.setAlignment(Pos.CENTER);

    //Displays picture if a picture is doubleclicked
    tableView.getTableView().setOnMousePressed(event -> {
        if((event.getClickCount() == 2) && event.isPrimaryButtonDown()) {
            mainController.showPicture(tableView);
        }
    });

    //Gets the searchbar
    HBox searchBar = makeSearchBar(tableView);
    searchBar.setAlignment(Pos.CENTER_RIGHT);

    //Gets the button box
    VBox buttonBox = makeButtons(tableView, primaryStage);

    //Makes the anchorPane and rootPane follow the stages size
    anchorPane.prefHeightProperty().bind(primaryStage.heightProperty());
    anchorPane.prefWidthProperty().bind(primaryStage.widthProperty());
    rootPane.prefHeightProperty().bind(anchorPane.heightProperty());
    rootPane.prefWidthProperty().bind(anchorPane.widthProperty());

    //Makes the rootpanes children nodes resize appropriately to the stage when width is changed
    rootPane.widthProperty().addListener(((observable, oldValue, newValueInt) -> {
      double newValue = (double) newValueInt;
      buttonBox.resize(newValue/8, buttonBox.getHeight());
      searchBar.resize(newValue, searchBar.getHeight());
      tableViewVbox.resize(newValue-searchBar.getWidth(), tableViewVbox.getHeight());
    }));

      //Makes the rootpanes children nodes resize appropriately to the stage when height is changed
      rootPane.heightProperty().addListener(((observable, oldValue, newValueint) -> {
      double newValue = (double) newValueint;
      buttonBox.resize(buttonBox.getWidth(), newValue);
      searchBar.resize(searchBar.getWidth(), newValue/8);
      tableViewVbox.resize(tableViewVbox.getWidth(), newValue-searchBar.getHeight());
    }));


    //Puts all nodes together
    rootPane.setTop(searchBar);
    rootPane.setLeft(buttonBox);
    rootPane.setCenter(tableViewVbox);
    anchorPane.getChildren().add(rootPane);
    Scene scene = new Scene(anchorPane);
    primaryStage.setScene(scene);

    //Sets minimum width of the stage and sets the stage to the
    //maximum resolution for the systems primary screen
    primaryStage.setMinWidth(650);
    primaryStage.setMinHeight(400);
    primaryStage.setHeight(Screen.getPrimary().getBounds().getMaxY());
    primaryStage.setWidth(Screen.getPrimary().getBounds().getMaxX());
    primaryStage.show();

    //Displays the default login alert if it goes to the default database
    if (showDefaultLogin){
      defaultLogin.show();
    }

  }

  @Override
  public void init() {

    this.mainController = new MainController();

    this.logger = Logger.getLogger(this.getClass().getName());

  }

  @Override
  public void stop() {
    System.exit(0);
  }


  private HBox makeSearchBar(ImageArchiveTableView tableView) {
    HBox hBox = new HBox();

    TextField searchInput = new TextField();
    searchInput.setEditable(true);
    searchInput.setPromptText("Search");
    searchInput.setTooltip(
            new Tooltip("Search for tags, title or metadata.\n"
                    + "For metadata search format as:\n"
                    + "metadata_variable: metadata_value")
    );
    searchInput.setMaxHeight(50);
    searchInput.setMinWidth(175);
    searchInput.setPromptText("Metadata:value, name or tag");

    Button submitSearch = new Button("Search");
    submitSearch.setOnAction(actionEvent -> {
      try {
        mainController.doSearchAll(searchInput.getText().toLowerCase(), this.pictures, tableView);
      } catch (IllegalArgumentException e) {
        logger.log(Level.INFO, e.getMessage());
        tableView.updateObservableList();
      }
    });
    submitSearch.setMaxHeight(50);

    Label tableViewResize = new Label();
    tableViewResize.setText("Slide columns to resize contents");
    tableViewResize.setFont(new Font(25));
    tableViewResize.setPadding(new Insets(0,20,0,0));

    hBox.widthProperty().addListener((observable, oldValue, newValueInt) -> {
      double newValue = (double) newValueInt;
      submitSearch.prefWidth(newValue/4);
      searchInput.prefWidth(2*newValue/4);
    });
    submitSearch.prefHeightProperty().bind(hBox.heightProperty());
    searchInput.prefHeightProperty().bind(hBox.heightProperty());

    hBox.getChildren().addAll(tableViewResize,searchInput,submitSearch);
    return hBox;
  }

  private VBox makeButtons(ImageArchiveTableView tableView, Stage primaryStage) {
    VBox vbox = new VBox();
    vbox.setMinSize(100,100);

    //Creates remove picture button, fixes it to the vboxes size and sets it to mainController.removePicture
    Button removePicture = new Button("Remove\npicture");
    removePicture.setOnAction(actionEvent -> {
      if (tableView.getTableView().getSelectionModel().getSelectedItem() != null) {
        mainController.removePicture(tableView, pictures);
      } else {
        noPictureDialog();
      }
    });
    removePicture.prefHeightProperty().bind(vbox.heightProperty());
    removePicture.prefWidthProperty().bind(vbox.widthProperty());

    //Creates add picture button, fixes it to the vboxes size and sets it to mainController.addPicture
    Button addPicture = new Button("Add\npicture");
    addPicture.setOnAction(actionEvent -> {
      mainController.addPicture(pictures, primaryStage, tableView);
      tableView.updateObservableList();
    });
    addPicture.prefHeightProperty().bind(vbox.heightProperty());
    addPicture.prefWidthProperty().bind(vbox.widthProperty());

    //Creates create pdf button, fixes it to the vboxes size and sets it to mainController.createPdf
    Button createPdf = new Button("Create\nPDF");
    createPdf.setOnAction(actionEvent -> mainController.createPdf(this.pictures));
    createPdf.prefHeightProperty().bind(vbox.heightProperty());
    createPdf.prefWidthProperty().bind(vbox.widthProperty());

    //Creates show picture button, fixes it to the vboxes size and sets it to mainController.showPicture
    Button showPicture = new Button("Show\npicture");
    showPicture.setOnAction(actionEvent -> {
      if (tableView.getTableView().getSelectionModel().getSelectedItem() != null) {
        mainController.showPicture(tableView);
      } else {
        noPictureDialog();
      }
    });
    showPicture.prefHeightProperty().bind(vbox.heightProperty());
    showPicture.prefWidthProperty().bind(vbox.widthProperty());

    //Creates add tag button, fixes it to the vboxes size and sets it to mainController.addTag
    Button addTag = new Button("Add\ntag");
    addTag.setOnAction(actionEvent -> {
      if (tableView.getTableView().getSelectionModel().getSelectedItem() != null) {
        mainController.doAddTag(tableView, pictures);
        tableView.updateObservableList();
      } else {
        noPictureDialog();
      }
    });
    addTag.prefHeightProperty().bind(vbox.heightProperty());
    addTag.prefWidthProperty().bind(vbox.widthProperty());

    //Adds all buttons to the vbox
    vbox.getChildren().addAll(addPicture, removePicture, addTag, createPdf, showPicture);

    //Makes the button texts resize according to the vboxes width
    vbox.widthProperty().addListener((observable, oldValue, newValueNumb) -> {
      double newValue = (double) newValueNumb;
      double newFont = newValue/7;
      addPicture.setFont(Font.font(newFont));
      removePicture.setFont(Font.font(newFont));
      addTag.setFont(Font.font(newFont));
      createPdf.setFont(Font.font(newFont));
      showPicture.setFont(Font.font(newFont));
      });

    return vbox;
  }

  private void noPictureDialog() {
    Alert noPicture = new Alert(Alert.AlertType.INFORMATION);
    noPicture.initModality(Modality.APPLICATION_MODAL);
    noPicture.setTitle("No picture selected");
    noPicture.setContentText("You have to select an image for this function");
    noPicture.show();
  }
}
