package photoAppProject.view;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import photoAppProject.model.ImageArchive;
import photoAppProject.model.Picture;

/**
 * Wrapper class for TableView for ease of use in PhotoApp development
 */
public class ImageArchiveTableView {
  private ArrayList<Picture> archive;
  private TableView<Picture> tableView;
  private ObservableList<Picture> observableArchive;
  private ImageArchive ia;
  private final Logger logger;
  private TableColumn thumbNailColumn;
  private TableColumn titleColumn;

  /**
   * Constructor for object ImageArchiveTableView.
   *
   * @param ia The image archive to display in the tableview.
   */

  public ImageArchiveTableView(ImageArchive ia) {
    this.ia = ia;
    this.archive = ia.getPictures();
    this.tableView = new TableView<>();
    this.observableArchive = getObservablePicturesList();
    this.logger = Logger.getLogger(getClass().getName());
  }
//  public ImageArchiveTableView(ImageArchiveAL iaDAO) {
//    this.archive = iaDAO.getPictures();
//    this.tableView = new TableView<>();
//    this.observableArchive = getObservablePicturesList();
//    this.logger = Logger.getLogger(getClass().getName());
//  }

  public void setHeight(double newHeight) {
    tableView.resize(tableView.getWidth(), newHeight);
    //tableView.resizeColumn(titleColumn, newHeight/3);
    //tableView.resizeColumn(thumbNailColumn, 2*newHeight/3);
  }

  public void setWidth(double newWidth) {
    tableView.resize(newWidth, tableView.getHeight());
    //tableView.resizeColumn(titleColumn, newWidth/3);
    //tableView.resizeColumn(thumbNailColumn, 2*newWidth/3);
  }

    /**
     * Updates the list of pictures
     *
     * @return an updated list
     */
  private ObservableList<Picture> getObservablePicturesList() {
    observableArchive =
        FXCollections.observableArrayList(this.archive);
    return observableArchive;
  }

  /**
   * Updates the list used in the tableview after some action is done to the list.
   */

  public void updateObservableList() {
    logger.log(Level.INFO, "Updating observable list");
    this.archive = ia.getPictures();
    this.observableArchive.setAll(this.archive);
  }

  /**
   * Changes the list used in the tableview.
   *
   * @param newList the new list to display in the tableview.
   */

  public void setObservableList(List newList) {
    this.observableArchive.setAll(newList);
  }

  public TilePane getTilePaneTableView() {
    TableColumn<Picture, ImageView> thumbnailColumn = new TableColumn<>("Thumbnail");
    thumbnailColumn.setMinWidth(100);
    thumbnailColumn.setCellValueFactory(new PropertyValueFactory<>("thumbnail"));

    TableColumn<Picture, String> titleColumn = new TableColumn<>("Name");
    titleColumn.setMinWidth(150);
    titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));

    this.titleColumn = titleColumn;
    this.thumbNailColumn = thumbnailColumn;

    this.tableView.setItems(this.getObservablePicturesList());
    this.tableView.getColumns().addAll(thumbnailColumn, titleColumn);
    this.tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    TilePane tilePane = new TilePane();
    tilePane.getChildren().add(this.tableView);

    return tilePane;
  }

  public VBox pdfCenterContent() {
    VBox vbox = new VBox();


    TableColumn<Picture, ImageView> thumbnailColumn = new TableColumn<>("Thumbnail");
    thumbnailColumn.setMinWidth(100);
    thumbnailColumn.setCellValueFactory(new PropertyValueFactory<>("PDFThumbnail"));

    TableColumn<Picture, String> titleColumn = new TableColumn<>("Name");
    titleColumn.setMinWidth(150);
    titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));

    this.titleColumn = titleColumn;
    this.thumbNailColumn = thumbnailColumn;

    this.tableView.setItems(this.getObservablePicturesList());
    this.tableView.getColumns().addAll(thumbnailColumn, titleColumn);
    //this.tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

    vbox.getChildren().add(this.tableView);

    return vbox;
  }

  /**
   * Creates the content to be placed in center borderPane.
   * @return VBox used in center BorderPane.
   */

  public VBox centerContent() {
    VBox vbox = new VBox();


    TableColumn<Picture, ImageView> thumbnailColumn = new TableColumn<>("Thumbnail");
    thumbnailColumn.setMinWidth(100);
    thumbnailColumn.setCellValueFactory(new PropertyValueFactory<>("thumbnail"));

    TableColumn<Picture, String> titleColumn = new TableColumn<>("Name");
    titleColumn.setMinWidth(150);
    titleColumn.setCellValueFactory(new PropertyValueFactory<>("nameText"));

    this.titleColumn = titleColumn;
    this.thumbNailColumn = thumbnailColumn;

    this.tableView.setItems(this.getObservablePicturesList());
    this.tableView.getColumns().addAll(thumbnailColumn, titleColumn);
    this.tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

    thumbnailColumn.widthProperty().addListener((observable, oldValue, newValueInt) -> {
          double newValue = (double) newValueInt;
          Picture.setImageWidth(newValue/2);
          updateObservableList();
      });

    titleColumn.widthProperty().addListener((observable, oldValue, newValueInt) -> {
        double newValue = (double) newValueInt;
        Picture.setTextFont(newValue/7);
        updateObservableList();
    });

    vbox.getChildren().add(this.tableView);

    tableView.prefHeightProperty().bind(vbox.heightProperty());

    titleColumn.prefWidthProperty().bind(tableView.widthProperty().divide(2));
    thumbnailColumn.prefWidthProperty().bind(tableView.widthProperty().divide(2));

    return vbox;
  }

  public TableView<Picture> getTableView(){
    return this.tableView;
  }


}
