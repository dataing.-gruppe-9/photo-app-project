package photoAppProject.view;

import java.io.IOException;
import java.util.Iterator;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import photoAppProject.model.ImageArchiveAL;
import photoAppProject.model.ImageArchive;
import photoAppProject.model.Picture;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

/**
 * Dialogue for setting up and printing PDF
 */
public class PDFDialogue extends Dialog<ImageArchive> {

  private ImageArchive archive;
  private ImageArchive selected;

  /**
   * Dialogue for printing your pictures to PDF
   *
   * @param archive the archive you want to print pictures from
   */
  public PDFDialogue(ImageArchive archive) {
    super();
    this.archive = new ImageArchiveAL();
    Iterator<Picture> it = archive.iterator();
    while (it.hasNext()) {
      this.archive.addPicture(it.next());
    }

    this.selected = new ImageArchiveAL();
    createContent();
  }

  private void createContent() {
    ButtonType createPDF = new ButtonType("Create PDF", ButtonBar.ButtonData.OK_DONE);
    getDialogPane()
        .getButtonTypes()
        .addAll(createPDF, ButtonType.CANCEL);
    getDialogPane().lookupButton(createPDF).setDisable(true);

    ImageArchiveTableView archiveTable = new ImageArchiveTableView(this.archive);
    ImageArchiveTableView selectedTable = new ImageArchiveTableView(this.selected);
    TextField fileName = new TextField();
    fileName.setPromptText("Title");
    fileName.setEditable(true);
    fileName.textProperty().addListener((observable, oldValue, newValue) -> {
      if (!newValue.trim().isEmpty()) {
        getDialogPane().lookupButton(createPDF).setDisable(false);
      } else {
        getDialogPane().lookupButton(createPDF).setDisable(true);
      }
    });

    BorderPane rootPane = new BorderPane();
    GridPane tablePanes = new GridPane();
    GridPane titlePane = new GridPane();
    tablePanes.add(archiveTable.pdfCenterContent(), 0, 0);
    tablePanes.add(selectedTable.pdfCenterContent(), 2, 0);

    archiveTable.getTableView().setOnMousePressed(mouseEvent -> {
      if (mouseEvent.isPrimaryButtonDown() && (mouseEvent.getClickCount() == 2)) {
        Picture selectedPicture = archiveTable
            .getTableView()
            .getSelectionModel()
            .getSelectedItem();
        archive.remove(selectedPicture);
        selected.addPicture(selectedPicture);
        archiveTable.updateObservableList();
        selectedTable.updateObservableList();
      }
    });

    selectedTable.getTableView().setOnMousePressed(mouseEvent -> {
      if (mouseEvent.isPrimaryButtonDown() && (mouseEvent.getClickCount() == 2)) {
        Picture selectedPicture = selectedTable
            .getTableView()
            .getSelectionModel()
            .getSelectedItem();
        selected.remove(selectedPicture);
        archive.addPicture(selectedPicture);
        archiveTable.updateObservableList();
        selectedTable.updateObservableList();
      }
    });
    CheckBox printMetadata = new CheckBox();
    CheckBox printTags = new CheckBox();
    GridPane checkBoxPane = new GridPane();
    checkBoxPane.add(new Label("Double click pictures to print in the selected order"),1,0);

    checkBoxPane.add(printMetadata, 0, 1);
    checkBoxPane.add(new Label("Do you want to print metadata in the pdf?"), 1, 1);

    checkBoxPane.add(printTags, 0, 2);
    checkBoxPane.add(new Label("Do you want to print tags in the pdf?"), 1, 2);

    rootPane.setCenter(tablePanes);
    rootPane.setBottom(checkBoxPane);
    titlePane.setAlignment(Pos.CENTER);
    titlePane.add(new Label("Add a Title "), 0, 0);
    titlePane.add(fileName, 1, 0);
    rootPane.setTop(titlePane);
    getDialogPane().setContent(rootPane);

    setResultConverter((ButtonType button) -> {
      ImageArchive result = this.selected;

      if (button.getButtonData() == ButtonBar.ButtonData.OK_DONE) {
        System.out.println("done");
        try {
          String location = createPdf(printMetadata.isSelected(), printTags.isSelected(), fileName.getText());
          Alert printSuccess = new Alert(Alert.AlertType.INFORMATION);
          printSuccess.setTitle("PDF printed");
          printSuccess.setContentText("Pdf printed to " + location);
          printSuccess.show();
        } catch (Exception e) {
          //TODO: add logger
          System.out.println(e.getMessage());
        }
      }

      return result;
    });


  }

  private String createPdf(boolean printMetadata, boolean printTags, String fileName) throws IOException {
    PDDocument doc = new PDDocument();


    Iterator<Picture> it = selected.iterator();
    while (it.hasNext()) {
      Picture picture = it.next();

      PDPage newPage = new PDPage();
      doc.addPage(newPage);

      PDPageContentStream cont = new PDPageContentStream(doc, newPage);

      PDImageXObject pdImage = PDImageXObject.createFromFile(picture.getPath(), doc);

      PDRectangle mediaBox = newPage.getMediaBox();
      int ratio = pdImage.getHeight() / pdImage.getWidth();

      int newSizex = (int)(PDRectangle.A4.getWidth() - 100);
      int newSizeY = (ratio * ((int) mediaBox.getWidth() - 100));

      float startX = ((mediaBox.getWidth()/2) - (newSizex/2));
      float startY = (mediaBox.getHeight() - newSizeY - 25);

      if (pdImage.getWidth() >= mediaBox.getWidth()) {
        cont.drawImage(pdImage, startX, startY,newSizex,newSizeY);
      } else {
        startX = (mediaBox.getWidth()-pdImage.getWidth())/2;
        startY = mediaBox.getHeight()-pdImage.getHeight()-25;
        cont.drawImage(pdImage, startX, startY);
      }


      if (printMetadata) {
        printMetadata(cont, startY, picture);

      }
      if(printTags){
        printTags(cont,startY,mediaBox.getWidth()/2,picture);
      }
      cont.close();

    }
    String saveLocation = System.getProperty("user.home") + System.getProperty("file.separator") +"Desktop"+
            System.getProperty("file.separator")+ fileName + ".pdf";

    doc.save(saveLocation);

    System.out.println(System.getProperty("user.home"));
    System.out.println("pdf created");

  return saveLocation;
  }

  private void printMetadata(PDPageContentStream cont, float startY, Picture picture) throws IOException {
    cont.beginText();

    cont.setFont(PDType1Font.TIMES_ROMAN, 12);
    cont.setLeading(14.5f);

    cont.newLineAtOffset(25, startY - 100);

    cont.showText("Metadata: ");
    cont.newLine();
    for (String metadata : picture.metadataStrings()) {
      cont.showText(metadata + " ");
      cont.newLine();
    }

    cont.newLine();
    cont.endText();

  }

  private void printTags(PDPageContentStream cont, float startY, float startX, Picture picture) throws IOException {
    cont.beginText();

    cont.setFont(PDType1Font.TIMES_ROMAN, 12);
    cont.setLeading(14.5f);

    cont.newLineAtOffset(startX, startY - 100);

    cont.showText("Tags: ");
    cont.newLine();
    for (String tag : picture.getTags()) {
      cont.showText(tag + " ");
      cont.newLine();
    }

    cont.newLine();
    cont.endText();

  }


}
