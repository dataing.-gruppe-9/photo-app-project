package photoAppProject.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public interface ImageArchive extends Iterable {
    ArrayList<Picture> getPictures();


    void addPicture(String picturePath);

    void addPicture(Picture picture);


    List anySearch(String searchWord);

    void remove(Picture picture);

    @Override
    Iterator<Picture> iterator();

    List<Picture> getPictureList();

    void addTag(Picture picture, String tag);
}
