package photoAppProject.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * previous placeholder class for datastorage
 * Now used for the PDF dialog
 */
public class ImageArchiveAL implements ImageArchive {
  private ArrayList<Picture> pictures;
  private static ImageArchive instance;

  private Logger logger;


    /**
     * Constructor for plain ImageArchiveAL
     */
  public ImageArchiveAL() {
    this.pictures = new ArrayList<>();
    this.logger = Logger.getLogger(this.getClass().getName());
  }

    /**
     *
     * @return the image archives list of pictures
     */
  @Override
  public ArrayList<Picture> getPictures() {
    return this.pictures;
  }

    /**
     * Adds new image to the list, metadata gets fetched
     * @param picturePath path of the picture to add
     */
  @Override
  public void addPicture(String picturePath) {
    try {
      Picture p = new Picture(picturePath);
      addPicture(p);
    } catch (Exception e) {
      logger.log(Level.WARNING, e.getMessage());
    }
  }

  /**
   * Adds image to whichever method we use to save.
   *
   * @param picture new picture to add.
   */
  @Override
  public void addPicture(Picture picture) {
    try {
      pictures.add(picture);
    } catch (Exception e) {
      logger.log(Level.WARNING, e.getMessage());
    }
  }

  /**
   * searches through archive for matching metadata,tag or title.
   *
   * @param searchWord the word to search for.
   * @return list containing Pictures with elements matching the searchword.
   */
  @Override
  public List anySearch(String searchWord) {
    List foundPicture = new ArrayList<>();
    Iterator<Picture> it = iterator();

    while (it.hasNext()) {
      Picture picture = it.next();
      if (picture.containsTag(searchWord)
          || picture.containsMetadata(searchWord)
          || picture.hasTitle(searchWord)) {
        foundPicture.add(picture);
      }

    }

    return foundPicture;
  }


    /**
     * Removes a picture from the internal list
     * @param picture to remove
     */
  @Override
  public void remove(Picture picture) {
    pictures.remove(picture);
  }

    /**
     *
     * @return iterator for image archives pictures
     */
  @Override
  public Iterator<Picture> iterator() {
    return pictures.iterator();
  }

    /**
     *
     * @return the image archives list of pictures
     */
  @Override
  public List<Picture> getPictureList() {
    return pictures;
  }

    /**
     * No use
     * @param picture
     * @param tag
     */
  @Override
  public void addTag(Picture picture, String tag) {

  }
}
