package photoAppProject.model;

import javafx.util.Pair;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Updated ImageArchive now using a database to store image paths
 */
public class ImageArchiveDAO implements ImageArchive {
    private ArrayList<Picture> pictures;
    private Logger logger;
    private final EntityManagerFactory entityManagerFactory;
    private final EntityManager entityManager;

    private void addTestFiles() {
        this.addPicture("testFiles/image.jpeg");
        this.addPicture("testFiles/test.gif");
        this.addPicture("testFiles/test4.bmp");
        this.addPicture("testFiles/test8.bmp");
        this.addPicture("testFiles/test8.png");
        this.addPicture("testFiles/test8.tif");
        this.addPicture("testFiles/test24.bmp");
        this.addPicture("testFiles/test24.png");
        this.addPicture("testFiles/test24.tif");
    }

    /**
     * Constructs an instance of imagearchiveDAO
     * Does also load images into the list of pictures
     */
    public ImageArchiveDAO () {
        this.entityManagerFactory = Persistence.createEntityManagerFactory("imagearchivedb");
        this.entityManager = entityManagerFactory.createEntityManager();
        this.logger = Logger.getLogger(getClass().getName());
        this.pictures = loadDatabase();
    }

    /**
     * Makes the application connect to the users database instead
     * of the default database
     *
     * @param username username for NTNU MYSQL username and URL
     * @param password password to log into NTNY MYSQL
     */
    public ImageArchiveDAO(String username, String password) {
        HashMap hm = new HashMap<String,String>();
        hm.put("javax.persistence.jdbc.url", "jdbc:mysql://mysql-ait.stud.idi.ntnu.no/"+username);
        hm.put("javax.persistence.jdbc.user", username);
        hm.put("javax.persistence.jdbc.password", password);
        this.entityManagerFactory = Persistence.createEntityManagerFactory("imagearchivedb",hm);
        this.entityManager = entityManagerFactory.createEntityManager();
        this.logger = Logger.getLogger(getClass().getName());
        this.pictures = loadDatabase();
    }

    /**
     * Getter for the picture list
     *
     * @return ArrayList of all pictures in the database
     */
    @Override
    public ArrayList<Picture> getPictures() {
        return this.pictures;
    }

    /**
     * Creates picture to add to both the Arraylist used for runtime storage
     * and the database and sends it to the method that handles the actual picture object
     *
     * @param picturePath path to the picture to add
     */
    @Override
    public void addPicture(String picturePath) {
        try {
            Picture p = new Picture(picturePath);
            addPicture(p);
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage());
        }
    }

    /**
     * Adds picture to the runtime ArrayList and the database
     *
     * @param picture picture to add
     */
    @Override
    public void addPicture(Picture picture) {
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(picture);
            entityManager.getTransaction().commit();
            this.pictures.add(picture);
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage());
        }
    }

    /**
     * Searches through all pictures' metadata and tags for a given searchword
     *
     * @param searchWord Word to search for
     * @return List of pictures with matching searchwords
     */
    @Override
    public List anySearch(String searchWord) {
        List foundPicture = new ArrayList<>();
        Iterator<Picture> it = iterator();

        while (it.hasNext()) {
            Picture picture = it.next();
            if (picture.containsTag(searchWord)
                || picture.containsMetadata(searchWord)
                || picture.hasTitle(searchWord)) {
                foundPicture.add(picture);
            }

        }

        return foundPicture;
    }

    /**
     * Removes the picture from the runtime Arraylist and the database
     *
     * @param picture picture to remove
     */
    @Override
    public void remove(Picture picture) {
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(picture);
            entityManager.getTransaction().commit();
            this.pictures.remove(picture);
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage());
        }
    }

    /**
     * getter for an iterator for the picturelist
     *
     * @return iterator of runtime picture list
     */
    @Override
    public Iterator<Picture> iterator() {
        return loadDatabase().iterator();
    }

    /**
     * Returns the ArrayList
     *
     * @return the the arraylist
     */
    @Override
    public List<Picture> getPictureList() {
        return this.pictures;
    }

    /**
     * Loads the whole database into an ArrayList used in runtime
     *
     * @return ArrayList containing all pictures
     */
    private ArrayList<Picture> loadDatabase() {
        ArrayList<Picture> dbList = new ArrayList<>();
        String jpql = new String("SELECT p FROM Picture p");
        Query query = entityManager.createQuery(jpql);
        List<Picture> pictureList = query.getResultList();
        Iterator it = pictureList.iterator();
        while (it.hasNext()) {
            Picture p = (Picture) it.next();
            dbList.add(p);
        }
        return dbList;
    }

    /**
     * Adds tag to picture and syncs arraylist to database
     *
     * @param picture picture to add tag to
     * @param tag tag to add to picture
     */
    @Override
    public void addTag(Picture picture, String tag) {
        Picture pic = entityManager.find(Picture.class, picture.getId());
        entityManager.getTransaction().begin();
        pic.addTag(tag);
        entityManager.getTransaction().commit();

        this.pictures = loadDatabase();
        }
}
