package photoAppProject.model;


import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageInputStream;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * MetaData class that fetches and holds all metadata from a picture
 */
public class MetaData implements Serializable {

  private String imgPath;
  private HashMap<String, String> metadataMap;
  transient private Logger logger;


  /**
   * Constructs an object of type MetaData.
   *
   * @param path path from where the metadata will be read.
   */
  public MetaData(String path) {
    try {
      if (path == null || path.trim().length() == 0) {
        throw new IllegalArgumentException("Path is null");
      }
      this.imgPath = path;
      this.metadataMap = new HashMap<>();
      this.logger = Logger.getLogger(this.getClass().getName());
      processFile();
    } catch (Exception e) {
      logger.log(Level.WARNING, e.getMessage());
    }
  }

  private String getFileExtension(File file) {
    String fileName = file.getName();
    int lastDot = fileName.lastIndexOf('.');
    return fileName.substring(lastDot + 1);
  }

  /**
   * Processes the image to get its metadata.
   *
   * @throws IOException thrown when unable to get readers.
   */

  public void processFile() throws IOException {
    logger.log(Level.INFO, this.imgPath);
    File file = new File(this.imgPath);
    logger.log(Level.INFO,file.getAbsolutePath());
    logger.log(Level.INFO, ("\n Processing " + file.getName() + ":\n"));

    String extension = getFileExtension(file);

    Iterator<ImageReader> readers = ImageIO.getImageReadersByFormatName(extension);

    while (readers.hasNext()) {
      ImageReader reader = readers.next();

      logger.log(Level.INFO, ("Reader: " + reader.getClass().getName()));

      processFileWithReader(file, reader);
    }

  }

  private void processFileWithReader(File file, ImageReader reader){
    ImageInputStream stream = null;
    IIOMetadata metadata = null;

    try {
      stream = ImageIO.createImageInputStream(file);

      reader.setInput(stream, true);

      metadata = reader.getImageMetadata(0);

      logger.log(Level.INFO, ("Image metadata"));
      String[] names = metadata.getMetadataFormatNames();

      for (String name : names) {
        logger.log(Level.INFO, ("Format name: " + name));
        storeMetadata(metadata.getAsTree(name));
      }

      metadata = reader.getStreamMetadata();
      if (metadata != null) {
        logger.log(Level.INFO, ("Stream metadata"));
        names = metadata.getMetadataFormatNames();

        for (String name : names) {
          logger.log(Level.INFO, ("Format name: " + name));
          storeMetadata(metadata.getAsTree(name));
        }

      }

    } catch (NullPointerException e) {
      logger.log(Level.WARNING, e.getMessage());
    } catch (IOException e){
      logger.log(Level.WARNING,e.getMessage());
    }finally {
      if (stream != null) {
        try {
          stream.close();
        }catch (IOException e){
          logger.log(Level.WARNING,e.getMessage());
        }
      }
      if (metadata == null) {
        logger.log(Level.INFO, ("No metadata available"));
      }
    }
  }

  private void storeMetadata(Node node) {
    NamedNodeMap attributes = node.getAttributes();
    if (attributes != null) {
      for (int i = 0; i < attributes.getLength(); i++) {
        Node attribute = attributes.item(i);
        this.metadataMap.put(attribute.getNodeName().toLowerCase(), attribute.getNodeValue());
      }
    }

    Node child = node.getFirstChild();
    if (child == null) {
      String value = node.getNodeValue();
      if (value == null || value.length() == 0) {
        throw new NullPointerException("Node has no value");
      } else {
        this.metadataMap.put(node.getNodeName().toLowerCase(), value);
      }
      return;
    }
    while (child != null) {
      storeMetadata(child);
      child = child.getNextSibling();
    }
  }

  /**
   * Searches through the metadata hashmap, both metadata id and value must match matches.
   *
   * @param search the string used in the search.
   *               Must be in the format: "Metadata_id: Metadata_value".
   * @return true if metadata has metadata of the id with the right value, otherwise false.
   */

  public boolean searchMetadata(String search) {
    boolean containMetadata = false;
    String searchWord = null;
    if (search == null || search.trim().length() == 0) {
      throw new IllegalArgumentException("Search parameter is invalid");
    }
    if (search.contains(":")) {
      searchWord = search;
      if(search.contains(" ")){
        searchWord = search.replace(" ","");
      }
      String dataKey = searchWord.substring(0, search.indexOf(':'));
      String dataValue = searchWord.substring(search.indexOf(':') + 1);

      if (metadataMap.containsKey(dataKey.toLowerCase())) {
        if (metadataMap.get(dataKey).equalsIgnoreCase(dataValue)) {
          containMetadata = true;
        } else {
          containMetadata = false;
        }

      }
    }

    return containMetadata;

  }

  /**
   *
   * @return a pictures metadata as a list of strings
   */
  public ArrayList<String> stringMetadata(){
    ArrayList<String> allMetadata = new ArrayList<>();
    for(String dataKey : metadataMap.keySet()){
      allMetadata.add(""+ dataKey+": "+ metadataMap.get(dataKey));
    }
    return allMetadata;
  }
}
