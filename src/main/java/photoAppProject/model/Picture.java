package photoAppProject.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Picture class to represent individual pictures with tags and metadata
 */
@Entity
public class Picture implements Serializable {
  @Id
  @GeneratedValue
  private Integer id;

  private String path;

  private MetaData metaData;

  @ElementCollection
  private List<String> tags;

  private static double imageWidth = 63;
  private static double textFont = 24;

  /**
   * Creates plain picture
   */
  public Picture () {
      this.tags = new ArrayList<>();
      if (path != null) {
        this.metaData = new MetaData(path);
      }
  }

  /**
   *
   * @return pictures ID number
   */
  public Integer getId() {
    return id;
  }

  /**
   *
   * @param id number
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * Sets the images path and fetches its metadata
   * @param path to image on pc
   */
  public void setPath(String path) {
    this.path = path;
    try {
      this.metaData = new MetaData(path);
      this.metaData.processFile();
    } catch (Exception e) {
    }
  }

  /**
   *
   * @param newFont new fontsize for the getNameText method
   */
  public static void setTextFont(double newFont) {
    if(newFont < 100) {
      textFont = newFont;
    } else {
      textFont = 100;
    }
  }

  /**
   * Creates new picture object and fetches metadata
   * @param path to picture
   */
  public Picture(String path){
    try {
      if ((path == null) || (path.trim().length() == 0)) {
        throw new IllegalArgumentException("Path does not contain the location of a picture");
      }
      this.path = path;
      this.metaData = new MetaData(path);
      this.metaData.processFile();
      tags = new ArrayList<>();
    } catch (Exception e) {
    }
  }

  /**
   *
   * @param tags list for already existing tags
   */
  public void setTags(List<String> tags) {
    this.tags = tags;
  }

  /**
   *
   * @return full path
   */
  public String getPath() {
    return this.path;
  }

  /**
   *
   * @return pictures title as a string
   */
  public String getTitle(){
    int stringStart = 0;
    if(this.path.contains("/")) {
      stringStart = this.path.lastIndexOf("/") + 1;
    }else if (this.path.contains("\\")){
      stringStart = this.path.lastIndexOf("\\")+1;
    }
    int stringEnd = this.path.lastIndexOf(".");
    return path.substring(stringStart,stringEnd);
  }

  /**
   * Adds tag to the tag list
   *
   * @param tag Tag to be added to tag list
   */
  public void addTag(String tag) {
    tags.add(tag.toUpperCase());
  }

  /**
   *
   * @return metadata object for this picture
   */
  public MetaData getMetaData() {
    return this.metaData;
  }

  /**
   *
   * @return tags list in form of an ArrayList
   */
  public ArrayList<String> getTags() {
    ArrayList<String> tagList = new ArrayList<>();
   tags.forEach(s -> tagList.add(s));
    return tagList;
  }

  /**
   * @return Title with fontsize tied to the fontSize field
   */
  public Text getNameText() {
    Text nameText = new Text();
    nameText.setText(getTitle());
    nameText.setFont(new Font(textFont));
    return nameText;
  }

  /**
   * Checks if picture contains the tag entered
   *
   * @param tag tag to check for
   * @return boolean value, if it contains the parameter tag it returns true, if else it returns false
   */
  public boolean containsTag(String tag) {
    boolean hasTag = false;
    hasTag = tags.stream()
        .anyMatch(s -> s.equalsIgnoreCase(tag));

    return hasTag;
  }

  /**
   *
   * @param search Metadata to search for
   * @return true if picture contains the right metadata
   */
  public boolean containsMetadata(String search){
    return metaData.searchMetadata(search);
  }

  /**
   *
   * @param search Title to check for
   * @return true if the search title is correct
   */
  public boolean hasTitle(String search){
    return getTitle().equalsIgnoreCase(search);
  }

  /**
   *
   * @param newWidth new image width
   */
  public static void setImageWidth(double newWidth) {
    imageWidth = newWidth;
  }

  /**
   * Returns image based on the class' static imagewidth value
   *
   * @return image for thumbnail
   */
  public ImageView getThumbnail(){
    ImageView imageView = new ImageView(new Image("file:"+this.path));
    imageView.setFitHeight(9*imageWidth/16);
    imageView.setFitWidth(imageWidth);
    return imageView;
  }

  /**
   *
   * @return image at a set size (64x32)
   */
  public ImageView getPDFThumbnail(){
    ImageView imageView = new ImageView(new Image("file:"+this.path));
    imageView.setFitHeight(32);
    imageView.setFitWidth(64);
    return imageView;
    }

  /**
   *
   * @return metadata information as an arraylist of strings
   */
  public ArrayList<String> metadataStrings(){
    return this.metaData.stringMetadata();
  }

  /**
   *
   * @return the list containing tags
   */
  public List<String> getTagsList() {
    return this.tags;
  }

}
