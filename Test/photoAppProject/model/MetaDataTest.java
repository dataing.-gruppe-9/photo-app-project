package photoAppProject.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MetaDataTest extends Picture {

    Picture p;

    @BeforeEach
    void setUp() {
        p = new Picture("testFiles/test4.bmp");
    }

    @Test
    void searchMetadata() {
       assertTrue(p.getMetaData().searchMetadata("height:56"));

    }
}