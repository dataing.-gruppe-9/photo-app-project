package photoAppProject.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ImageArchiveDAOTest {
  private ImageArchiveDAO imgDAO;
  public ImageArchiveDAOTest(){
    this.imgDAO = new ImageArchiveDAO();

  }

  @Test
  public void picturesLoaded(){
    assertEquals(true,imgDAO.getPictures().size()>=1);
  }

  @Test
  public void addPictureTest(){
    int i = imgDAO.getPictures().size();
    imgDAO.addPicture("./testFiles/test.gif");
    assertEquals(i+1,imgDAO.getPictures().size());
  }

  @Test
  public void removeTest(){
    int i = imgDAO.getPictures().size();
    Picture p1 = (Picture)imgDAO.anySearch("test").get(0);
    imgDAO.remove(p1);
    assertEquals(i-1,imgDAO.getPictures().size());

  }

}