package photoAppProject.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ImageArchiveALTest {

  private ImageArchiveAL imgAL;

  public ImageArchiveALTest(){
    this.imgAL = new ImageArchiveAL();
  }

  @Test
  public void addPictureTest(){
    this.imgAL.addPicture("./testFiles/test.gif");
    assertEquals(1,imgAL.getPictures().size());
    try {
      this.imgAL.addPicture(new Picture("./testFiles/test.gif"));
      assertEquals(2,imgAL.getPictures().size());
    } catch (Exception e){
      System.out.println(e.getMessage());
    }
  }

  @Test
  public void anySearchTest(){
    this.imgAL.addPicture("./testFiles/test.gif");
    this.imgAL.addPicture("./testFiles/image.jpeg");
    this.imgAL.addPicture("./testFiles/test4.bmp");
    this.imgAL.addPicture("./testFiles/test8.bmp");
    this.imgAL.addPicture("./testFiles/test24.png");
    Picture p1 = (Picture)imgAL.anySearch("test").get(0);
    p1.addTag("someTag");
    assertEquals("test",p1.getTitle());

    Picture p2 = (Picture)imgAL.anySearch("someTag").get(0);
    assertEquals("test",p2.getTitle());
  }

  @Test
  public void removeTest(){
    this.imgAL.addPicture("./testFiles/test.gif");
    this.imgAL.addPicture("./testFiles/image.jpeg");
    this.imgAL.addPicture("./testFiles/test4.bmp");
    this.imgAL.addPicture("./testFiles/test8.bmp");
    this.imgAL.addPicture("./testFiles/test24.png");

    imgAL.remove(imgAL.getPictures().get(0));

    assertEquals(4,imgAL.getPictures().size());
  }

}