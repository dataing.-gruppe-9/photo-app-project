package photoAppProject.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PictureTest {
  private Picture picture;

  public PictureTest(){
    try {
      picture = new Picture("./testFiles/test.gif");
    } catch(Exception e){
      System.out.println(e.getMessage());
    }
  }

  @Test
  public void titleTest(){
    assertEquals(true,picture.hasTitle("test"));
    try {
      picture.setPath("./testFiles/test8.bmp");
    } catch(Exception e){
      System.out.println(e.getMessage());
    }
    assertEquals(true,picture.hasTitle("test8"));
  }

  @Test
  public void tagTest(){
    picture.addTag("someTag");
    assertEquals(true,picture.containsTag("someTag"));
  }


}